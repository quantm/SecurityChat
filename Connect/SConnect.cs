﻿/*******************************
//
//      SERVER CONNECTION
//
*******************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Text;
using System.Xml;
using System.Security.Cryptography;
namespace SecurityChat
{
    public class StateObject
    {
        public Socket workSocket = null;
        public const int BufferSize = 10000;
        public byte[] buffer = new byte[BufferSize];
        public StringBuilder sb = new StringBuilder();
    }
    public class SocketClient
    {
        public struct Chating
        {
            public Socket sendSocket;
            public string SendToNameID;
            public string SharedEncryptKey;
        }
        public Socket socket;
        public string NameID;
        public string EncryptKey;
        public string SessionHash; //md5
        public List<Chating> ChatList = new List<Chating>();
        
    }
    public class CConnect
    {
        private const int BUFFER_SIZE = 1024;
        private CDataAccess Data = new CDataAccess();
        private static ManualResetEvent allDone = new ManualResetEvent(false);
        private ManualResetEvent EventLogon;
        private Socket server;
        private LinkedList<SocketClient> SocketList = new LinkedList<SocketClient>();
        private static string recvString;
        private string address;
        private int port;
        public CConnect(string addr, int portnum) 
        {
            this.address = addr;
            this.port = portnum;
            EventLogon = new ManualResetEvent(false);
        }
        public void startListening()
        {
            Thread thread = new Thread(new ThreadStart(this.Listening));
            thread.Start();
        }
        private void Listening()
        {
            IPEndPoint iep = new IPEndPoint(IPAddress.Parse(address), this.port);
            this.server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                this.server.Bind(iep);
                this.server.Listen(10);
                while (true)
                {
                    allDone.Reset();
                    this.server.BeginAccept(new AsyncCallback(acceptCallback), server);
                    allDone.WaitOne();
                }
            }
            catch (SocketException e) { MessageBox.Show(e.Message);}

        }
      
        /*Phân tích yêu cầu từ client*/
        private bool ParseClientRequest(Socket client, string content)
        {
            /*
             * Phân tích yêu cầu từ client
             * return true nếu yêu cầu hợp lệ
             */
         
            if (content.IndexOf("<Logon>") != -1
                && content.IndexOf("</Logon>") != -1)
            {
                this.ParseLogonRequest(client, content);
                return true;
            }
            if (content.IndexOf("<Message>") != -1
                && content.IndexOf("</Message>") != -1)
            {
                this.ParseMessageRequest(client, content);
                return true;
            }
            if (content.IndexOf("<Register>") != -1
                && content.IndexOf("</Register>") != -1)
            {
                ///MessageBox.
                ParseRegisterRequest(client, content);
                return true;
            }
            if (content.IndexOf("<NewEncryptKey>") != -1
                && content.IndexOf("</NewEncryptKey>") != -1)
            {
                ParseNewEncryptKey(client, content);
            }
            return false;
            
        }
        private void ParseMessage(string content, ref string NameID
            , ref string Session, ref string SendToNameID, ref string msg)
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.LoadXml(content);
                NameID = doc.GetElementsByTagName("NameID")[0].InnerXml;
                Session = doc.GetElementsByTagName("Session")[0].InnerXml;
                SendToNameID = doc.GetElementsByTagName("SendTo")[0].InnerXml;
                msg = doc.GetElementsByTagName("Msg")[0].InnerXml;
            }
            catch (XmlException e)
            {
                MessageBox.Show(e.Message);
            }
        }
        private void ParseNameIDAndPassword(string content,ref string NameID, ref string Password)
        {
            //Lấy ra NameID và mật khẩu từ phía người dùng
            XmlDocument doc = new XmlDocument();
            try
            {

                doc.LoadXml(content);
                NameID = doc.GetElementsByTagName("NameID")[0].InnerXml;
                Password = doc.GetElementsByTagName("Password")[0].InnerXml;
            }
            catch (XmlException e)
            {
                MessageBox.Show(e.Message);
            }

        }
        
        private int ParseLogonRequest(Socket client, string content)
        {
            string NameID = "", Password = "";
            this.ParseNameIDAndPassword(content, ref NameID, ref Password);
            if (NameID == "" || Password == "") return 1;
            string EncryptKey = Data.QueryEncryptKey(NameID);
            if (EncryptKey != "")
            {
                Password = AESEncrypto.Decrypt(Password, EncryptKey);
                if (Password == "")
                {
                    SendDataToClient(client, "<Logon><Message>IncorrectEncryptKey</Message><Session>0</Session></Logon>");
                    return 1;
                }
            }
            else
            {
                SendDataToClient(client, "<Logon><Message>IncorrectEncryptKey</Message><Session>0</Session></Logon>");
                return 1;
            }
            bool Valid = Data.QueryUserAndPassWord(NameID, Password);
            if (Valid)
            {
                string Session = CreateSession(200);
                SocketClient skl = new SocketClient();
                skl.NameID = NameID;
                skl.socket = client;
                skl.EncryptKey = EncryptKey;
                skl.SessionHash = CalculateMD5Hash(Session);
                SocketList.AddFirst(skl);
                string Msg = String.Format("<Logon><Message>Success</Message><Session>{0}</Session></Logon>"
                    , Session);
                SendDataToClient(client, Msg);
                return 0;
            }
            else
            {
                SendDataToClient(client, "<Logon><Message>Failed</Message><Session>0</Session></Logon>");
                return 1;
            }
        }

        private int ParseMessageRequest(Socket client, string content)
        {
            string NameID = "", Session = "", SendToNameID = "", msg = "";
            ParseMessage(content, ref NameID, ref Session, ref SendToNameID, ref msg);
            string eKey = Data.QueryEncryptKey(NameID);
            string eKey2 = Data.QueryEncryptKey(SendToNameID);
            msg = AESEncrypto.Decrypt(msg, eKey);
            msg = AESEncrypto.Encrypt(msg, eKey2);
            string Message = String.Format("<Message><SendFrom>{0}</SendFrom><Session>0"
                                + "</Session><Msg>{1}</Msg></Message>", NameID, msg);
           

            int Valid = 0; bool bInChatList = false;
            SocketClient sTemp = new SocketClient();
            foreach (SocketClient s in SocketList)
            {
                /*Kiểm tra session hợp lệ*/
                if (s.socket == client)
                {
                    if (s.SessionHash == Session)
                    {
                        Valid = 1;
                        sTemp = s;
                        for (int i = 0; i < s.ChatList.Count; i++)
                        {
                            if (s.ChatList[i].SendToNameID == SendToNameID)
                            {
                                bInChatList = true;
                                if (s.ChatList[i].sendSocket.Connected)
                                    SendDataToClient(s.ChatList[i].sendSocket, Message);
                                else
                                {
                                    s.ChatList.RemoveAt(i);
                                }
                            }
                        }
                        break;
                    }
                }
            }
            if (Valid == 1)
            {
                if (!bInChatList)
                {
                    foreach (SocketClient skl in SocketList)
                    {
                        if (skl.NameID == SendToNameID)
                        {
                            SocketClient.Chating chat = new SocketClient.Chating();
                            chat.sendSocket = skl.socket;
                            chat.SendToNameID = SendToNameID;
                            sTemp.ChatList.Add(chat);
                            SendDataToClient(skl.socket, Message);
                            break;
                        }
                    }
                }
            }
            return 0;
        }

        private int ParseRegisterRequest(Socket client, string content)
        {
            string NameID="", Password="", Email="", Address="", TelephoneNum="", EncryptKey="", Session="";
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.LoadXml(content);
                NameID = doc.GetElementsByTagName("NameID")[0].InnerXml;
                Password = doc.GetElementsByTagName("Password")[0].InnerXml;
                Email = doc.GetElementsByTagName("Email")[0].InnerXml;
                Address = doc.GetElementsByTagName("Address")[0].InnerXml;
                TelephoneNum = doc.GetElementsByTagName("TelephoneNum")[0].InnerXml;
                EncryptKey = doc.GetElementsByTagName("EncryptKey")[0].InnerXml;
                Session = doc.GetElementsByTagName("Session")[0].InnerXml;
            }
            catch (XmlException e)
            {
                MessageBox.Show(e.Message);
            }
            /*Giải mã thông tin đăng kí bằng public key*/
            NameID = RSAEncrypto.DecryptData(NameID);
            Password = RSAEncrypto.DecryptData(Password);
            Email = RSAEncrypto.DecryptData(Email);
            Address = RSAEncrypto.DecryptData(Address);
            TelephoneNum = RSAEncrypto.DecryptData(TelephoneNum);
            EncryptKey = RSAEncrypto.DecryptData(EncryptKey);
            Session = RSAEncrypto.DecryptData(Session);
            int res = Data.AddUserName(NameID, Password, Email, Address, TelephoneNum
                , EncryptKey, 0);
            if (res == 1)
            {
                string Msg = String.Format("<Register><Message>AlreadyExists</Message>"
                    + "<Session>{0}</Session></Register>", CreateSession(20));
                SendDataToClient(client, Msg);
            }
            if (res == 0) /*Nếu đăng kí thành công*/
            {
                Session = CreateSession(200);
                SocketClient skl = new SocketClient();
                skl.NameID = NameID;
                skl.socket = client;
                skl.SessionHash = CalculateMD5Hash(Session);
                SocketList.AddFirst(skl);
                string Msg = String.Format("<Register><Message>RegisterSuccess</Message>"
                    + "<Session>{0}</Session></Register>", CreateSession(20));
                SendDataToClient(client, Msg);
            }
            return 0;
        }

        private int ParseNewEncryptKey(Socket client, string content)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(content);
            string Session = doc.GetElementsByTagName("Session")[0].InnerXml;
            string NameID = doc.GetElementsByTagName("NameID")[0].InnerXml;
            string SendTo = doc.GetElementsByTagName("SendTo")[0].InnerXml;
            string EncryptKey = doc.GetElementsByTagName("EncryptKey")[0].InnerXml;
            return 0;
        }
        /*Phân tích yêu cầu từ client (END)*/

        private void SendDataToClient(Socket client, string sdata)
        {
            byte[] data = new byte[BUFFER_SIZE];
            data = Encoding.UTF8.GetBytes(sdata);
            client.Send(data, data.Length, SocketFlags.None);
        }
        private static string CreateSession(int lenght)
        {
            string name = "";
            Random r = new Random();
            for (int i = 0; i < lenght; i++)
                name += (char)r.Next(97, 122);
            return name;
        }
        private static string CalculateMD5Hash(string input)
        {
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
  

        /*
         *Đồng bộ các client kết nối đến 
         * 
         * 
        */
        public void acceptCallback(IAsyncResult ar)
        {        
            Socket listener = (Socket)ar.AsyncState;
            Socket handler = listener.EndAccept(ar);
            allDone.Set();
            StateObject state = new StateObject();
            state.workSocket = handler;
            try
            {
                handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(readCallback), state);
            }
            catch (SocketException e) { MessageBox.Show(e.Message); }
        }
        public void readCallback(IAsyncResult ar)
        {

            StateObject state = (StateObject)ar.AsyncState;
            Socket handler = state.workSocket;
            SocketClient node = new SocketClient();
            foreach (SocketClient skl in SocketList)
            {
                if (skl.socket == handler)
                    node = skl;
            }
            try
            {
                int read = handler.EndReceive(ar);

                // Đọc dữ liệu từ socket
                if (read > 0)
                {
                    state.sb.Clear();
                    state.sb.Append(Encoding.UTF8.GetString(state.buffer, 0, read));
                    handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                        new AsyncCallback(readCallback), state);
                    recvString = state.sb.ToString();
                    ParseClientRequest(handler, recvString);
                        //handler.Close();
                    
                }
                else
                {
                    if (state.sb.Length > 1)
                    {
                        string content = state.sb.ToString();

                    }
                    handler.Close();
                }
            }
            catch (SocketException e)
            {
                SocketList.Remove(node);
               // MessageBox.Show(node.NameID + " is disconnected");
                e.Data.Clear();
                handler.Close();
            }

        }

        private Socket FindTargetSocket(Socket client, string Session, string SendToNameID)
        {
            SocketClient sTemp = new SocketClient();
            int Valid = 0; bool bInChatList = false;
            foreach (SocketClient s in SocketList)
            {
                /*Kiểm tra session hợp lệ*/
                if (s.socket == client)
                {
                    if (s.SessionHash == Session)
                    {
                        Valid = 1;
                        sTemp = s;
                        for (int i = 0; i < s.ChatList.Count; i++)
                        {
                            if (s.ChatList[i].SendToNameID == SendToNameID)
                            {
                                bInChatList = true;
                                if (s.ChatList[i].sendSocket.Connected)
                                    return s.ChatList[i].sendSocket;//, Message);
                                else
                                {
                                    s.ChatList.RemoveAt(i);
                                }
                            }
                        }
                        break;
                    }
                }
            }
            if (Valid == 1)
            {
                if (!bInChatList)
                {
                    foreach (SocketClient skl in SocketList)
                    {
                        if (skl.NameID == SendToNameID)
                        {
                            SocketClient.Chating chat = new SocketClient.Chating();
                            chat.sendSocket = skl.socket;
                            chat.SendToNameID = SendToNameID;
                            sTemp.ChatList.Add(chat);
                            //SendDataToClient(skl.socket, Message);
                            return skl.socket;
                            //break;
                        }
                    }
                }
            }
            return null;
        }
    
    }
}