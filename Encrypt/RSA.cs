﻿using System.Text;
using System.IO;
using System.Security.Cryptography;
using System;
namespace SecurityChat
{
    class RSAEncrypto
    {
        private static RSACryptoServiceProvider m_rsaProvider;
        public static void AssignParameter()
        {
            const int PROVIDER_RSA_FULL = 1;
            const string CONTAINER_NAME = "RSAContainer";
            CspParameters cspParams;
            cspParams = new CspParameters(PROVIDER_RSA_FULL);
            cspParams.KeyContainerName = CONTAINER_NAME;
            cspParams.Flags = CspProviderFlags.UseMachineKeyStore;
            cspParams.ProviderName = "Microsoft Strong Cryptographic Provider";
            m_rsaProvider = new RSACryptoServiceProvider(10000, cspParams);
          //  ((KeySize - 384) / 8) + 37
        }
        public static void AssignNewKey()
        {
            AssignParameter();
         
            StreamWriter swWriter = new StreamWriter(@"privatekey.xml");
            string strPublicPrivateKeyXML = m_rsaProvider.ToXmlString(true);
            swWriter.Write(strPublicPrivateKeyXML);
            swWriter.Close();
       
            swWriter = new StreamWriter(@"publickey.xml");
            string strPublicOnlyKeyXML = m_rsaProvider.ToXmlString(false);
            swWriter.Write(strPublicOnlyKeyXML);
            swWriter.Close();
        }
        public static string EncryptData(string strData2Encrypt)
        {
            AssignParameter();
            StreamReader srReader = new StreamReader(@"publickey.xml");
            string strPublicOnlyKeyXML = srReader.ReadToEnd();
            m_rsaProvider.FromXmlString(strPublicOnlyKeyXML);
            srReader.Close();
            //read plaintext, encrypt it to ciphertext
            byte[] baPlainbytes = System.Text.Encoding.UTF8.GetBytes(strData2Encrypt);
            byte[] baCipherbytes = m_rsaProvider.Encrypt(baPlainbytes, false);
            return Convert.ToBase64String(baCipherbytes);
        }
        public static string DecryptData(string strData2Decrypt)
        {
            AssignParameter();
            byte[] baGetPassword = Convert.FromBase64String(strData2Decrypt);
            StreamReader reader = new StreamReader(@"privatekey.xml");
            string publicPrivateKeyXML = reader.ReadToEnd();
            m_rsaProvider.FromXmlString(publicPrivateKeyXML);
            reader.Close();
            //read ciphertext, decrypt it to plaintext
            byte[] baPlain = m_rsaProvider.Decrypt(baGetPassword, false);
            return System.Text.Encoding.UTF8.GetString(baPlain);
        }

    }
}

/*
 Sử dụng RSA
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSA_Encryption_and_Decryption
{
    class Program
    {
        static void Main(string[] args)
        {
            RSAEngine.AssignNewKey();
            string strClearText = "qtm";
            string strEncryptText = RSAEngine.EncryptData(strClearText);
            Console.WriteLine(strEncryptText);
            string strDecryptText = RSAEngine.DecryptData(strEncryptText);
            Console.WriteLine(strClearText);
            Console.ReadLine();
        }
    }
}
*/