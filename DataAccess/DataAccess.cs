﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Objects;
using System.Data.Common;
namespace SecurityChat
{
    class CDataAccess
    {

        private SecurityChatEntities data;
        public CDataAccess()
        {
            data = new SecurityChatEntities();
        }
        private bool IsNameIDExist(string NameID)
        {
            ObjectQuery<UserName> u = data.UserNames.Where("it.NameID == '"+ NameID+"'");
            List<UserName> uList = new List<UserName>();
            uList = u.ToList();
            if (uList.Count == 0)
                return false;
            return true;
            
        }
        private int GetMaxValueID()
        {
            try
            {
                ObjectQuery<UserName> u = data.UserNames.Where("it.No != 0");
                List<UserName> ulist = new List<UserName>();
                ulist = u.ToList();
                return ulist[ulist.Count - 1].No;
            }
            catch (ArgumentException e)
            {
                MessageBox.Show("Lỗi khi lấy giá trị max của ID.\n" + e.Message);
                return -1;
            }
        }
        public int AddUserName(string NameID, string PassWord
            , string Email, string Address, string TelephoneNum
            , string EncryptKey, int Online) 
        {
            if (IsNameIDExist(NameID))
            {
                return 1;//failed
            }
            int No = this.GetMaxValueID();
            if (No == -1) return 2; // failed
            No += 1;
            UserName user = new UserName
            {
                No = No,
                NameID = NameID,
                PassWord = PassWord,
                Email = Email,
                Address = Address,
                TelephoneNum = TelephoneNum,
                EncryptKey = EncryptKey,
                Online = Online
            };
            data.UserNames.AddObject(user);
            data.SaveChanges();
            return 0;
        }
        public bool QueryUserAndPassWord(string NameID, string PassWord)
        {
            //return true if success
            try
            {
                ObjectQuery<UserName> obj
                = data.UserNames.Where("it.NameID == '" + NameID + "' AND it.PassWord == '" + PassWord + "'");
                List<UserName> list = obj.ToList();
                if (list.Count == 1) return true;
                else
                    return false;
            }
            catch (ArgumentException e)
            {
                MessageBox.Show("Lỗi khi truy vấn NameID và mật khẩu.\n" + e.Message);
            }
            return false;
          //  data.ExecuteStoreCommand(
        }
        public string QueryEncryptKey(string NameID)
        {
            try
            {
                ObjectQuery<UserName> obj
                    = data.UserNames.Where("it.NameID == '" + NameID + "'");
                List<UserName> list = obj.ToList();
                if (list.Count == 1)
                {
                    return list[0].EncryptKey;
                }
            }
            catch (ArgumentException e)
            {
                MessageBox.Show("Lỗi khi truy vấn EncryptKey.\n" + e.Message);
            }
            return "";
        }
    }
}